package com.alinesno.cloud.portal.desktop.web.ucenter;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.controller.BaseController;

/**
 * 显示设置
 * @author LuoAnDong
 * @since 2019年4月19日 上午6:18:05
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("portal/ucenter")
public class UcenterController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(UcenterController.class) ; 

	@Value("${server.servlet.context-path:/}")
	private String serlvetContextPath ; 
	
	@GetMapping(value = "home")
	public void home(Model model , HttpServletRequest request) {
		log.debug("serlvetContextPath:{}" , serlvetContextPath);
	}
	
}
