package com.alinesno.cloud.portal.desktop.web.repository;

import java.util.List;

import com.alinesno.cloud.common.core.orm.repository.IBaseJpaRepository;
import com.alinesno.cloud.portal.desktop.web.entity.ModuleEntity;

/**
 * <p>
  * 内容模块 持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
public interface ModuleRepository extends IBaseJpaRepository<ModuleEntity, String> {

	List<ModuleEntity> findAllByMenusId(String menusId);

	List<ModuleEntity> findAllByModuleParentId(String moduleParentId);

	List<ModuleEntity> findAllByMenusIdAndModuleParentId(String menusId, String parentId);

	List<ModuleEntity> findAllByMenusIdAndModuleParentIdOrderByModuleSortAsc(String menusId, String parentId);

}
