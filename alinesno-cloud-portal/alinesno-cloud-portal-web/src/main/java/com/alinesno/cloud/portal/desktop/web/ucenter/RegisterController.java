package com.alinesno.cloud.portal.desktop.web.ucenter;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.controller.BaseController;
import com.alinesno.cloud.common.web.base.response.ResponseBean;
import com.alinesno.cloud.common.web.login.aop.AccountRecord;
import com.alinesno.cloud.common.web.login.aop.AccountRecord.RecordType;
import com.alinesno.cloud.portal.desktop.web.bean.RegisterBean;

/**
 * 显示设置
 * @author LuoAnDong
 * @since 2019年4月19日 上午6:18:05
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("public/portal/ucenter")
public class RegisterController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(RegisterController.class) ; 

	/**
	 * 用户注册
	 * @param model
	 * @param request
	 */
	@AccountRecord(type=RecordType.LOGIN_REGISTER)
	@GetMapping(value = "register")
	public String register(Model model , HttpServletRequest request) {
		log.debug("进入注册页面.");
		
		return "/portal/ucenter/register" ; 
	}

	/**
	 * 用户注册
	 * @param model
	 * @param request
	 * @return
	 */
	@ResponseBody
	@AccountRecord(type=RecordType.OPERATE_SAVE)
	@PostMapping(value = "register")
	public ResponseBean registerSave(Model model , HttpServletRequest request , RegisterBean registerBean) {
		log.debug("注册实体:{}." , ToStringBuilder.reflectionToString(registerBean));
		
		
	
		return ok() ; 
	}


}
