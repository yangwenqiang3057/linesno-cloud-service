package com.alinesno.cloud.portal.desktop.web.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.portal.desktop.web.bean.ModuleBean;
import com.alinesno.cloud.portal.desktop.web.entity.ModuleEntity;
import com.alinesno.cloud.portal.desktop.web.repository.ModuleRepository;
import com.alinesno.cloud.portal.desktop.web.service.IModuleService;

import cn.hutool.core.bean.BeanUtil;

/**
 * <p>
 * 内容模块 服务实现类
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
@Service
public class ModuleServiceImpl extends IBaseServiceImpl<ModuleRepository, ModuleEntity, String>
		implements IModuleService {

	// 日志记录
	private static final Logger log = LoggerFactory.getLogger(ModuleServiceImpl.class);

	@Override
	public List<ModuleBean> findAllByMenusId(String menusId) {
		Assert.hasLength(menusId, "菜单id不能为空.");
		List<ModuleEntity> parentModule = jpa.findAllByMenusIdAndModuleParentIdOrderByModuleSortAsc(menusId, "0");

		List<ModuleBean> mList = new ArrayList<ModuleBean>();

		for (ModuleEntity e : parentModule) {
			ModuleBean b = new ModuleBean();
			BeanUtil.copyProperties(e, b);

			List<ModuleEntity> subModule = jpa.findAllByModuleParentId(e.getId());
			b.setSubModules(subModule);

			mList.add(b);
		}

		return mList;
	}

	@Override
	public List<ModuleEntity> findAllByModuleParentId(String parentId) {
		if (parentId == null) {
			parentId = "0";
		}
		return jpa.findAllByModuleParentId(parentId);
	}

	@Override
	public boolean modifyDesignStatus(String id) {
		Assert.notNull(id,"主键不能为空.");
		ModuleEntity e = jpa.getOne(id) ; 
		e.setModuleDesign(((Integer.parseInt(e.getModuleDesign())+1)%2)+""); 
		e = jpa.save(e) ; 
		
		log.debug("newEntity:{} , hasStatus:{}" , e , e.getHasStatus());
		return true ;
	}

	
}
