package com.alinesno.cloud.portal.desktop.web.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.portal.desktop.web.entity.LinkPathEntity;
import com.alinesno.cloud.portal.desktop.web.repository.LinkPathRepository;
import com.alinesno.cloud.portal.desktop.web.service.ILinkPathService;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
@Service
public class LinkPathServiceImpl extends IBaseServiceImpl<LinkPathRepository, LinkPathEntity, String> implements ILinkPathService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(LinkPathServiceImpl.class);

}
