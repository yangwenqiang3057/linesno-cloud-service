package com.alinesno.cloud.common.web.login.token;

import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * 用户登陆信息
 * @author LuoAnDong
 * @sine 2019年4月5日 上午11:02:12
 */
@SuppressWarnings("serial")
public class LoginAccountToken extends UsernamePasswordToken {

	private String captcha ; //验证码
	private String key ; // 密钥登陆 

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	
}
