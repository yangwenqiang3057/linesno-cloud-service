package com.alinesno.cloud.common.web.enable;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

import com.alinesno.cloud.common.web.login.config.KaptchaConfig;
import com.alinesno.cloud.common.web.login.controller.KaptchaController;
import com.alinesno.cloud.common.web.login.controller.LoginController;
import com.alinesno.cloud.common.web.login.shiro.AccountRealm;
import com.alinesno.cloud.common.web.login.shiro.config.ShiroConfiguration;

/**
 * 引入自动类
 * @author LuoAnDong
 * @sine 2019年4月5日 下午3:34:07
 */
public class CommonLoginConfigurationSelector implements ImportSelector {

	@Override
	public String[] selectImports(AnnotationMetadata importingClassMetadata) {
		List<String> importBean = new ArrayList<String>() ; 

		// common core 
		List<String> coreLoader = WebImportProvider.classLoader() ; 
		importBean.addAll(coreLoader) ; 
	 
		// common login  
		importBean.add(KaptchaConfig.class.getName()) ; 
		importBean.add(KaptchaController.class.getName()) ; 
		importBean.add(ShiroConfiguration.class.getName()) ; 
		importBean.add(AccountRealm.class.getName()) ; 
		importBean.add(LoginController.class.getName()) ; 
				
		return importBean.toArray(new String[] {}) ;
	}

	
}

