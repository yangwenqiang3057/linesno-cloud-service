package com.alinesno.cloud.common.web.login.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.boot.feign.dto.ManagerAccountDto;
import com.alinesno.cloud.base.boot.feign.facade.ManagerAccountFeigin;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.utils.AStringUtils;
import com.alinesno.cloud.common.web.base.controller.BaseController;
import com.alinesno.cloud.common.web.base.monitor.Server;
import com.alinesno.cloud.common.web.base.response.ResponseBean;
import com.alinesno.cloud.common.web.base.response.ResponseGenerator;
import com.alinesno.cloud.common.web.login.session.CurrentAccountSession;

/**
 * 控制层
 * 
 * @author LuoAnDong
 * @since 2018年11月27日 上午6:41:40
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("dashboard/common/")
public class CommonController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(CommonController.class);

	@Autowired
	private ManagerAccountFeigin managerAccountFeign;

	@GetMapping("/user/avatar")
	public String userAvatar() {
		log.debug("进入用户头像编辑页面");
		return "common/user/avatar";
	}
	
	/**
	 *     更新用户头像
	 * @return
	 */
	@PostMapping("/user/avatar")
	public ResponseBean updateUserAvatar() {
		
		return ResponseGenerator.genSuccessResult("修改用户头像成功.");
	}

	@GetMapping("/user/profile")
	public String userProfile(ModelMap mmap, HttpServletRequest request) {
		log.debug("进入用户编辑页面");

		mmap.put("account", managerAccountFeign.getOne(CurrentAccountSession.get(request).getId()));

		return "common/user/profile";
	}

	@GetMapping("/user/resetPassword")
	public String resetPassword(ModelMap mmap, HttpServletRequest request) {
		log.debug("进入用户编辑页面");

		mmap.put("account", managerAccountFeign.getOne(CurrentAccountSession.get(request).getId()));

		return "common/user/resetPassword";
	}

	@ResponseBody
	@PostMapping("/user/resetPassword")
	public ResponseBean updatePassword(ModelMap mmap, HttpServletRequest request, String newPassword , String oldPassword) {
		log.debug("保存用户新修改的密码:{} , 旧密码:{}", newPassword , oldPassword);

		ManagerAccountDto user = CurrentAccountSession.get(request);

		if (AStringUtils.isNotEmpty(newPassword) && AStringUtils.isNotEmpty(oldPassword)) {
			
			if(oldPassword.equals(newPassword)) {
				return ResponseGenerator.genFailMessage("新旧密码不能相同.");
			}
			
			boolean b = managerAccountFeign.resetPassword(user.getId() , newPassword , oldPassword) ; 
			if(b) {
				return ResponseGenerator.genSuccessResult() ; 
			}
		} 
		
		return ResponseGenerator.genFailMessage("修改密码失败，旧密码错误");
	}

	@GetMapping("/monitor/server")
	public String monitorServer(ModelMap mmap) throws Exception {
		log.debug("进入服务监控页面");

		Server server = new Server();
		server.copyTo();
		mmap.put("server", server);

		return "common/monitor/server";
	}

}
