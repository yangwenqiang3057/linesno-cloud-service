package com.alinesno.cloud.common.core.feign;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.openfeign.support.PageJacksonModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.Module;

import feign.Logger;

/**
 * 日志配置
 * @author LuoAnDong
 * @since 2019年3月3日 下午6:21:26
 */
@Configuration
public class FeignConfig {

	/**
	 * 添加分页支持
	 * @return
	 */
	@Bean
    @ConditionalOnClass(name = {"org.springframework.data.domain.Page"})
	public Module pageJacksonModule() {
		return new PageJacksonModule();
	}
	
    @Bean
    Logger.Level feignLevel() {
        return Logger.Level.FULL;
    }
}