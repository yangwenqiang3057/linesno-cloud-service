package com.alinesno.cloud.compoment.code.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 项目应用技术
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Entity
@Table(name="project_framwork")
public class ProjectFramworkEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 技术编码
     */
	private String frameworkCode;
    /**
     * 项目编码
     */
	private String projectCode;


	public String getFrameworkCode() {
		return frameworkCode;
	}

	public void setFrameworkCode(String frameworkCode) {
		this.frameworkCode = frameworkCode;
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}


	@Override
	public String toString() {
		return "ProjectFramworkEntity{" +
			"frameworkCode=" + frameworkCode +
			", projectCode=" + projectCode +
			"}";
	}
}
