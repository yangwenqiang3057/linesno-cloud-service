package com.alinesno.cloud.compoment.code.devops.tools;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

/**
 * Created by lixin on 2018/10/12.
 */
public class WSTools {

	private static final Logger log = LoggerFactory.getLogger(WSTools.class) ; 
	
    private WebSocketSession webSocketSession;

    public WSTools(WebSocketSession webSocketSession) {
        this.webSocketSession = webSocketSession;
    }

    public void send(String msg) {
        try {
            if (this.webSocketSession.isOpen()) {
                this.webSocketSession.sendMessage(new TextMessage("# " + msg));
            } else {
                log.warn("WebSocket 连接已经关闭");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void send(String username, String host, String path, String msg) {
        try {
            if (this.webSocketSession.isOpen()) {
                this.webSocketSession.sendMessage(new TextMessage(username + "@" + host + ":" + path + "# " + msg));
            } else {
                log.warn("WebSocket 连接已经关闭");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
