package com.alinesno.cloud.compoment.code.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.compoment.code.entity.ModuleEntity;
import com.alinesno.cloud.compoment.code.repository.ModuleRepository;

/**
 * <p> 第三方模块池 服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@NoRepositoryBean
public interface IModuleService extends IBaseService<ModuleRepository, ModuleEntity, String> {

}
