package com.alinesno.cloud.compoment.code.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.compoment.code.entity.ModuleEntity;
import com.alinesno.cloud.compoment.code.repository.ModuleRepository;
import com.alinesno.cloud.compoment.code.service.IModuleService;

/**
 * <p> 第三方模块池 服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Service
public class ModuleServiceImpl extends IBaseServiceImpl<ModuleRepository, ModuleEntity, String> implements IModuleService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ModuleServiceImpl.class);

}
