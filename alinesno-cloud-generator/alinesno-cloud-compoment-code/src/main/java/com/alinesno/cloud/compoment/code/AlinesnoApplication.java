package com.alinesno.cloud.compoment.code;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing ;
import org.springframework.scheduling.annotation.EnableAsync;

import com.alinesno.cloud.base.boot.feign.enable.EnableAlinesnoBaseBoot;
import com.alinesno.cloud.common.web.enable.EnableAlinesnoCommonLogin;

/**
 * 启动入口<br/>
 * @EnableSwagger2 //开启swagger2 
 * 
 * @author LuoAnDong 
 * @sine 2019-06-29 12:06:539
 */
@EnableCaching
@EnableJpaAuditing // jpa注解支持
@EnableAsync // 开启异步任务
@EnableEurekaClient  // 开启eureka
@SpringBootApplication

@EnableAlinesnoBaseBoot
@EnableAlinesnoCommonLogin
public class AlinesnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlinesnoApplication.class, args);
	}

}
