package com.alinesno.cloud.compoment.code.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.compoment.code.entity.ProjectJobLogsEntity;
import com.alinesno.cloud.compoment.code.repository.ProjectJobLogsRepository;
import com.alinesno.cloud.compoment.code.service.IProjectJobLogsService;

/**
 * <p> 任务日志 服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Service
public class ProjectJobLogsServiceImpl extends IBaseServiceImpl<ProjectJobLogsRepository, ProjectJobLogsEntity, String> implements IProjectJobLogsService {

	//日志记录
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(ProjectJobLogsServiceImpl.class);

}
