package com.alinesno.cloud.compoment.code.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 版本控制管理
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Entity
@Table(name="project_repository_account")
public class ProjectRepositoryAccountEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 版本管理编码
     */
	private String code;
    /**
     * 项目编码
     */
	private String projectCode;
    /**
     * 帐户名
     */
	private String account;
    /**
     * 密码
     */
	private String password;
    /**
     * 仓库地址
     */
	private String home;
    /**
     * 仓库说明
     */
	private String description;
    /**
     * 状态：停用[Disenable]，启用[Enable]
     */
	private String state;
    /**
     * 仓库类型:Git, Svn
     */
	private String type;


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	@Override
	public String toString() {
		return "ProjectRepositoryAccountEntity{" +
			"code=" + code +
			", projectCode=" + projectCode +
			", account=" + account +
			", password=" + password +
			", home=" + home +
			", description=" + description +
			", state=" + state +
			", type=" + type +
			"}";
	}
}
