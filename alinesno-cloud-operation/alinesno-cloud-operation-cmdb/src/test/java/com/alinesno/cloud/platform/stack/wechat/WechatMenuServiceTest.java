package com.alinesno.cloud.platform.stack.wechat;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.operation.cmdb.third.wechat.WechatMenuService;
import com.alinesno.cloud.platform.stack.JUnitBase;
import com.google.gson.Gson;

import me.chanjar.weixin.common.bean.menu.WxMenuButton;

public class WechatMenuServiceTest extends JUnitBase {

	@Autowired
	private WechatMenuService wechatMenuService ; 
	
	@Test
	public void testCreateMenu() {
		
		//我的任务 - http://ad.linesno.com/my_task
		//接单 - http://ad.linesno.com/shop
		//我的订单 - http://ad.linesno.com/order_list
		//任务 - http://ad.linesno.com"
	
		List<WxMenuButton> buttons = new ArrayList<WxMenuButton>() ; 
		// ------------------ 任务_start --------------
		WxMenuButton task = new WxMenuButton() ; 
		task.setName("快速下单");
		task.setUrl("http://ad.linesno.com");
		task.setType("view");
		
		buttons.add(task) ; 
		// ------------------ 任务_end --------------
		
		// ------------------ 任务_start --------------
		WxMenuButton shop = new WxMenuButton() ; 
		shop.setName("店铺");
		shop.setUrl("http://ad.linesno.com/shop");
		shop.setType("view");
		
		buttons.add(shop) ; 
		// ------------------ 任务_end --------------
		
		
		// ------------------ 个人中心_start --------------
		WxMenuButton ucenter = new WxMenuButton() ; 
		ucenter.setName("我的");
		ucenter.setType("view");
		
		List<WxMenuButton> subButtons = new ArrayList<WxMenuButton>() ; 
		
		WxMenuButton taskList = new WxMenuButton() ; 
		taskList.setName("我的订单");
		taskList.setUrl("http://ad.linesno.com/order_list");
		taskList.setType("view");
		subButtons.add(taskList) ; 
		
		WxMenuButton myTask = new WxMenuButton() ; 
		myTask.setName("我的任务");
		myTask.setUrl("http://ad.linesno.com/my_task");
		myTask.setType("view");
		subButtons.add(myTask) ; 
		
		ucenter.setSubButtons(subButtons); 
		buttons.add(ucenter) ; 
		// ------------------ 个人中心_end --------------

		logger.debug(new Gson().toJson(buttons));
		
		boolean b = wechatMenuService.createMenu(buttons) ; 
		assertTrue(b);
	}

}
