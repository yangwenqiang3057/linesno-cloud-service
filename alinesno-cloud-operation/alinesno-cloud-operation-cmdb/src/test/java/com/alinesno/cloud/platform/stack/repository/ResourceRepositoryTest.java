package com.alinesno.cloud.platform.stack.repository;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.entity.ResourceEntity;
import com.alinesno.cloud.operation.cmdb.repository.ResourceRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ResourceRepositoryTest {

	@Autowired
	private ResourceRepository resourceRepository ; 

	@Test
	public void testSaveAllIterableOfS() {
		
//		BEGIN;
//		INSERT INTO `resources` VALUES 
//		('551482687840321536', '2019-03-02 19:15:19', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:15:19', 'fa-tachometer-alt', '/manager/main', '仪盘表', '10', null), 
//		('551482687907430400', '2019-03-02 19:15:19', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:15:19', 'fa-chart-area', '/manager/order_list', '申请管理', '8', null), 
//		('551482687915819008', '2019-03-02 19:15:19', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:15:19', 'fa-tags', '/manager/machine_list', '服务管理', '6', null), 
//		('551482687920013312', '2019-03-02 19:15:19', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:15:19', 'fa-user', '/manager/member_list', '会员管理', '1', null), 
//		('551482687924207616', '2019-03-02 19:15:19', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:15:19', 'fa-edit', '/manager/server_list', '主机管理', '5', null), 
//		('551482687928401920', '2019-03-02 19:15:19', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:15:19', 'fa-bell-slash', '/manager/params_list', '参数管理', '3', null), 
//		('551482687936790528', '2019-03-02 19:15:19', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:15:19', 'fa-table', '/manager/account_list', '账户管理', '1', null), 
//		('551482687915819007', '2019-03-02 19:15:19', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:15:19', 'fa-cubes', '/manager/deploy_list', '部署管理', '7', null), 
//		('551482687915819006', '2019-03-02 19:15:19', '0', null, null, null, '0', '0', null, '0', '2019-03-02 19:15:19', 'fa-link', '/manager/links_list', '链接管理', '6', null);
//		COMMIT;
		
		ResourceEntity m1 = new ResourceEntity(9 , "仪盘表" , "/manager/main" , "fa-tachometer-alt") ; 
		
		ResourceEntity m2 = new ResourceEntity(8 , "申请管理" , "/manager/order_list" , "fa-chart-area") ; 
		ResourceEntity m8 = new ResourceEntity(7 , "部署管理" , "/manager/deploy_list" , "fa-cubes") ; 
		ResourceEntity m9 = new ResourceEntity(6 , "链接管理" , "/manager/links_list" , "fa-link") ; 
		
		ResourceEntity m7 = new ResourceEntity(5 , "服务管理" , "/manager/machine_list" , "fa-tags") ; 
		ResourceEntity m4 = new ResourceEntity(4 , "主机管理" , "/manager/server_list" , "fa-edit") ; 
		
		ResourceEntity m5 = new ResourceEntity(3 , "参数管理" , "/manager/params_list" , "fa-bell-slash") ; 
		ResourceEntity m6 = new ResourceEntity(2 , "账户管理" , "/manager/account_list" , "fa-table") ; 
		ResourceEntity m3 = new ResourceEntity(1 , "会员管理" , "/manager/member_list" , "fa-user") ; 
		
		List<ResourceEntity> list = new ArrayList<ResourceEntity>() ; 
		list.add(m1) ; 
		list.add(m2) ; 
		list.add(m7) ; 
		list.add(m3) ; 
		list.add(m4) ; 
		list.add(m5) ; 
		list.add(m8) ; 
		list.add(m6) ; 
		list.add(m9) ; 
	
		resourceRepository.saveAll(list) ; 
	}

}
