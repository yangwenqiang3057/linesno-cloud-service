package com.alinesno.cloud.operation.cmdb.common.constants;

/**
 * 订单状态
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午6:41:23
 */
public enum MasterMachineEnum {

//	NOT_SCHOOL("0", "全国"), // 全国高校  
	ERROR("-1", "未知高校"); // 未知高校 

	public String code;
	public String text;

	MasterMachineEnum(String code, String text) {
		this.code = code;
		this.text = text;
	}

	public static MasterMachineEnum getStatus(String code) {
//		if ("0".equals(code)) {
//			return SchoolEnum.NOT_SCHOOL ;
//		} 
		return MasterMachineEnum.ERROR ;
	}

	public String getText() {
		return this.text;
	}

	public String getCode() {
		return this.code;
	}
}