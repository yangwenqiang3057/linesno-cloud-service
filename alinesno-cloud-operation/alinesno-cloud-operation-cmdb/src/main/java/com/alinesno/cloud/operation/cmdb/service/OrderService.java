package com.alinesno.cloud.operation.cmdb.service;

/**
 * 订单服务 
 * @author LuoAnDong
 * @since 2018年10月12日 上午6:22:21
 */
public interface OrderService {

	/**
	 * 删除订单
	 * @param orderId
	 */
	void deleteOrder(String orderId);

	/**
	 * 申请通过
	 * @param orderInfoId
	 * @param machineId
	 * @return
	 */
	boolean applyPass(String orderInfoId, String machineId);

}
