<%@ page pageEncoding="UTF-8"%>
<jsp:directive.include file="includes/top.jsp" />
<body data-app-state="">
	<div id="root">
		<div>
			<div class="zJwEi">
				<div class="eaYrSC">
					<div class="jCprYf"></div>
					<div class="iMoAjM">
						<div class="vHoXo">
							<header class="dMTsYF">
								 <jsp:directive.include file="includes/svg.jsp" />
								<h1>登陆你的账户</h1>
							</header>
							<section role="main" class="ffnCIW">
								<div class="cMZWRp">
									<div>
										<div role="alert" class="hidden bYGriA"></div>
										 <form:form method="post" id="fm1" commandName="${commandName}" htmlEscape="true">
										 <section class="row btn-row">
									      <input type="hidden" name="lt" value="${loginTicket}" />
									      <input type="hidden" name="execution" value="${flowExecutionKey}" />
									      <input type="hidden" name="_eventId" value="submit" />
									    </section>
										 	
										<div id="form-sign-up">
										 	<form:errors path="*" id="msg" cssClass="errors" element="div" htmlEscape="false" />
											<div class="  hRPdFp">
												<div>
													<label for="email" class="cQjUNh">
														<div class="cRWorA">
															<span>账户</span>
														</div>
													</label>
													<div class="eexybP">
														<div>
															<div>
																<div>
																	<div class="gnyRfH">
																		<div class="NYali">
																			<form:input cssClass="gcfMkP" cssErrorClass="error" id="username" size="25" tabindex="1" accesskey="${userNameAccessKey}" path="username" autocomplete="off" htmlEscape="true" />
																		</div>
																	</div>
																</div>
															</div>
															<div class="eftsfW">
																<div style="top: 0px; left: 0px; position: absolute; z-index: 400; opacity: 0;"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="  hRPdFp">
												<div>
													<div class="geHwRp">
														<div>
															<label for="password" class="cQjUNh">
																<div class="cRWorA">
																	<span>密码</span>
																</div>
															</label>
															<div class="eexybP">
																<div>
																	<div>
																		<div>
																			<div class="gnyRfH">
																				<div class="NYali">
																					<!-- 
																					<input type="password" id="password"
																						placeholder="你的密码" spellcheck="true" value=""
																						class="gcfMkP" />
																					 -->
																					<form:password cssClass="gcfMkP" cssErrorClass="error" id="password" size="25" tabindex="2" path="password"  accesskey="${passwordAccessKey}" htmlEscape="true" autocomplete="off" />
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="eftsfW">
																		<div
																			style="top: 0px; left: 0px; position: absolute; z-index: 400; opacity: 0;"></div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div>
													<p class="dvrYmd"></p>
												</div>
											</div>
											<p class="signup-legal daSVgz">
												<span>登陆隐私说明 <a
													href="https://www.atlassian.com/end-user-agreement"
													target="_blank" class="end-user-agreement">同意说明</a>   
												</span>
											</p>
											<div class="hSHNUR">
												<button class="koAzE ixXVkd"  spacing="default" id="signup-submit" type="submit">
													<span
														style="align-self: center; display: inline-flex; flex-wrap: nowrap; max-width: 100%; width: 100%; justify-content: center;"><span
														style="align-items: center; align-self: center; flex: 1 1 auto; margin: 0px 4px; max-width: 100%; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;"><span>
														<spring:message code="screen.welcome.button.login" />
														</span></span></span>
												</button>
											</div>
										</div>
										</form:form>
									</div>
								</div>
							</section>
							<jsp:directive.include file="includes/bottom.jsp" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

