package com.alinesno.cloud.base.boot.web.plugin;

import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alinesno.cloud.base.boot.feign.dto.ContentPostTypeDto;
import com.alinesno.cloud.base.boot.feign.dto.ManagerAccountDto;
import com.alinesno.cloud.base.boot.feign.facade.ContentPostTypeFeigin;
import com.alinesno.cloud.base.boot.feign.facade.ManagerAccountFeigin;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.advice.TranslatePlugin;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * 文章内容转换
 * @author LuoAnDong
 * @since 2019年4月7日 下午10:04:58
 */
@Component
public class ContentTranslatePlugin implements TranslatePlugin {

	@Autowired
	private ContentPostTypeFeigin contentPostTypeFeigin ; 

	@Autowired
	private ManagerAccountFeigin managerAccountFeigin ; 
	
	private String POSTMIMETYPE = "postMimeType" ; 
	private String POSTAUTHOR = "postAuthor" ; 
	
	@Override
	public void translate(ObjectNode node, TranslateCode convertCode) {
		JsonNode typeNode = node.get(POSTMIMETYPE) ; 
		
		String value = "" ; 
		if(!typeNode.isNull()) {
			log.debug("type node = {}" , typeNode.asText());
			if(StringUtils.isNotBlank(typeNode.asText())) {
				Optional<ContentPostTypeDto> dto = contentPostTypeFeigin.findById(typeNode.asText()) ; 
				if(dto.isPresent()) {
					value = dto.get().getTypeName() ;
				}
			}
		}
		node.put(POSTMIMETYPE + LABEL_SUFFER, value) ; 
		
		JsonNode authorTypeNode = node.get(POSTAUTHOR) ; 
		if(!authorTypeNode.isNull()) {
			log.debug("authorTypeNode = {}" , authorTypeNode.asText());
			
			Optional<ManagerAccountDto> dto = managerAccountFeigin.findById(authorTypeNode.asText()) ; 
			if(dto.isPresent()) {
				value = dto.get().getName() ;
			}
		}
		node.put(POSTAUTHOR + LABEL_SUFFER, value) ; 
	}

}
