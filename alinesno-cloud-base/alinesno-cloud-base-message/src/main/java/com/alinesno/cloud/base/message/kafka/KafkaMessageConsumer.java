package com.alinesno.cloud.base.message.kafka;

import org.springframework.stereotype.Component;

/**
 * kafka消息接收
 * 
 * @author LuoAnDong
 * @since 2018年12月17日 下午10:24:30
 */
@Component
public class KafkaMessageConsumer {

//    private static final Logger LOG = LoggerFactory.getLogger(KafkaMessageConsumer.class);
//    
//    @KafkaListener(topics={"transactin_message"})
//    public void receive(@Payload String message, @Headers MessageHeaders headers){
//    	
//        LOG.info("KafkaMessageConsumer 接收到消息："+message);
//        
//        headers.keySet().forEach(key->LOG.info("{}: {}",key,headers.get(key)));
//        
//    }
}