package com.alinesno.cloud.base.message.enums;

/**
 * 消息数据类型
 * @author LuoAnDong
 * @since 2018年12月1日 上午11:06:57
 */
public enum MessageDataTypeEnum {

	/**
	 * JSON格式
	 */
	JSON("json") , 
	
	/**
	 * XML数格式
	 */
	XML("xml") ; 
	
	private String value ; 
	
	private MessageDataTypeEnum(String v){
		value = v ; 
	}
	
	public String value(){
		return value ; 
	}
}
