/**
 * 消息收发器，针对于kafka的消息收发器，同时为了消息的持久化管理
 */
/**
 * @author LuoAnDong
 * @since 2018年11月20日 下午8:14:05
 */
package com.alinesno.cloud.base.message.kafka;