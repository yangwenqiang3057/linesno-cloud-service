package com.alinesno.cloud.base.logger.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
@SuppressWarnings("serial")
public class LogTypeDto extends BaseDto {

    /**
     * 类型代码
     */
	private String typeCode;
	
    /**
     * 日志类型名称
     */
	private String typeName;
	


	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

}
