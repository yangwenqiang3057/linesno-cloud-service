package com.alinesno.cloud.base.storage.repository;

import com.alinesno.cloud.base.storage.entity.StorageFileHistoryEntity;
import com.alinesno.cloud.common.core.orm.repository.IBaseJpaRepository;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-04-09 20:22:17
 */
public interface StorageFileHistoryRepository extends IBaseJpaRepository<StorageFileHistoryEntity, String> {

}
