package com.alinesno.cloud.base.boot.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.databene.contiperf.PerfTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.base.boot.entity.ManagerApplicationEntity;
import com.alinesno.cloud.base.boot.service.IManagerApplicationService;
import com.alinesno.cloud.common.core.junit.JUnitBase;

/**
 * 管理员
 * @author LuoAnDong
 * @since 2019年4月15日 下午10:11:28
 */
public class ManagerApplicationServiceImplTest extends JUnitBase {

	@Autowired
	private IManagerApplicationService managerApplicationService ; 
	
	@Test
	public void testFindAllByAccount() {
		String accountId = "567263477580693504" ; 
		List<ManagerApplicationEntity> as = managerApplicationService.findAllByAccountId(accountId) ; 
	
		for(ManagerApplicationEntity a : as) {
			log.debug("bean:{}" , ToStringBuilder.reflectionToString(a));
		}
		
	}

	@Test
	@PerfTest(invocations = 10000 , threads = 100) // 并发测试
	public void testFindById() {
		String id = "564392969700900864" ; 
		Optional<ManagerApplicationEntity> t = managerApplicationService.findById(id) ; 
		log.debug("bean: {}" , t);
	}

}
