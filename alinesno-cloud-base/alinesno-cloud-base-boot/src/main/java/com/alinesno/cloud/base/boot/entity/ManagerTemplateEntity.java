package com.alinesno.cloud.base.boot.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Entity
@Table(name="manager_template")
public class ManagerTemplateEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 模板名称
     */
	@Column(name="template_name")
	private String templateName;
    /**
     * 模板内容
     */
	@Column(name="template_content")
	private String templateContent;
    /**
     * 模板数据
     */
	@Column(name="template_data")
	private String templateData;
    /**
     * 模板时间
     */
	@Column(name="template_addtime")
	private Date templateAddtime;
    /**
     * 模板状态
     */
	@Column(name="template_status")
	private String templateStatus;
    /**
     * 所属菜单
     */
	@Column(name="resource_id")
	private String resourceId;
    /**
     * 模板作者
     */
	@Column(name="template_owner")
	private String templateOwner;


	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getTemplateContent() {
		return templateContent;
	}

	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}

	public String getTemplateData() {
		return templateData;
	}

	public void setTemplateData(String templateData) {
		this.templateData = templateData;
	}

	public Date getTemplateAddtime() {
		return templateAddtime;
	}

	public void setTemplateAddtime(Date templateAddtime) {
		this.templateAddtime = templateAddtime;
	}

	public String getTemplateStatus() {
		return templateStatus;
	}

	public void setTemplateStatus(String templateStatus) {
		this.templateStatus = templateStatus;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getTemplateOwner() {
		return templateOwner;
	}

	public void setTemplateOwner(String templateOwner) {
		this.templateOwner = templateOwner;
	}


	@Override
	public String toString() {
		return "ManagerTemplateEntity{" +
			"templateName=" + templateName +
			", templateContent=" + templateContent +
			", templateData=" + templateData +
			", templateAddtime=" + templateAddtime +
			", templateStatus=" + templateStatus +
			", resourceId=" + resourceId +
			", templateOwner=" + templateOwner +
			"}";
	}
}
