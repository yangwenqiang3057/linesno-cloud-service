package com.alinesno.cloud.base.boot.repository;

import com.alinesno.cloud.base.boot.entity.ManagerApplicationEntity;
import com.alinesno.cloud.common.core.orm.repository.IBaseJpaRepository;

/**
 * <p>
  *  持久层接口
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
public interface ManagerApplicationRepository extends IBaseJpaRepository<ManagerApplicationEntity, String> {

	/**
	 * 查询所有用户
	 * @param accountId
	 * @return
	 */
//	@Query("from ManagerApplicationEntity t1 where t1.id in (select applicationId from ManagerRoleEntity t2 where t2.id (:roleIds) group by t2.applicationId)")
//	List<ManagerApplicationEntity> findAllByRoleIds(@Param("roleIds") List<String> roleIds);

}
