package com.alinesno.cloud.base.boot.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.boot.entity.InfoJobEntity;
import com.alinesno.cloud.base.boot.repository.InfoJobRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@NoRepositoryBean
public interface IInfoJobService extends IBaseService<InfoJobRepository, InfoJobEntity, String> {

}
