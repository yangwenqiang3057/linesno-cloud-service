package com.alinesno.cloud.base.boot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@SuppressWarnings("serial")
@Entity
@Table(name="manager_account")
public class ManagerAccountEntity extends BaseEntity {

    /**
     * 所属者
     */
	private String owners;
    /**
     * 账户状态
     */
	@Column(name="account_status")
	private String accountStatus;
    /**
     * 最后登陆ip
     */
	@Column(name="last_login_ip")
	private String lastLoginIp;
    /**
     * 最后登陆时间
     */
	@Column(name="last_login_time")
	private String lastLoginTime;
    /**
     * 登陆名称
     */
	@Column(name="login_name" , unique=true)
	private String loginName;
    /**
     * 登陆密码
     */
	private String password;
    /**
     * 加密字符
     */
	private String salt;
    /**
     * 用户信息id
     */
	@Column(name="user_id")
	private String userId;
    /**
     * 所属角色
     */
	@Column(name="role_id")
	private String roleId;
    /**
     * 用户名称.
     */
	private String name;
    /**
     * 用户权限(9超级管理员/1租户权限/0用户权限)
     */
	@Column(name="role_power")
	private String rolePower;

	/**
	 * 所属部门
	 */
	private String department ;  

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	public String getOwners() {
		return owners;
	}

	public void setOwners(String owners) {
		this.owners = owners;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public String getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRolePower() {
		return rolePower;
	}

	public void setRolePower(String rolePower) {
		this.rolePower = rolePower;
	}


	@Override
	public String toString() {
		return "ManagerAccountEntity{" +
			"owners=" + owners +
			", accountStatus=" + accountStatus +
			", lastLoginIp=" + lastLoginIp +
			", lastLoginTime=" + lastLoginTime +
			", loginName=" + loginName +
			", password=" + password +
			", salt=" + salt +
			", userId=" + userId +
			", roleId=" + roleId +
			", name=" + name +
			", rolePower=" + rolePower +
			"}";
	}
}
