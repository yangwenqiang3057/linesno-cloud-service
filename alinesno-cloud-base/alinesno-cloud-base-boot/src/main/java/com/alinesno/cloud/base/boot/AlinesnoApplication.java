package com.alinesno.cloud.base.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;

import com.alinesno.cloud.common.core.auto.EnableAlinesnoCommonCore;

/**
 * 启动入口
 * @EnableSwagger2 //开启swagger2
 * @author LuoAnDong
 * @since 2018-12-16 17:12:901
 */
@EnableJpaAuditing
@EnableAsync
@EnableEurekaClient
@SpringBootApplication
@EnableAlinesnoCommonCore
@EnableCaching
public class AlinesnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlinesnoApplication.class, args);
	}


}
