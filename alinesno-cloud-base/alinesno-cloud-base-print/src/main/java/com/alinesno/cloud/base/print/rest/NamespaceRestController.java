package com.alinesno.cloud.base.print.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.base.print.entity.NamespaceEntity;
import com.alinesno.cloud.base.print.service.INamespaceService;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2019-05-03 14:07:38
 */
@Scope(SpringInstanceScope.PROTOTYPE)
@RestController
@RequestMapping("namespace")
public class NamespaceRestController extends BaseRestController<NamespaceEntity , INamespaceService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(NamespaceRestController.class);

}
