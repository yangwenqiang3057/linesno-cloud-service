package com.alinesno.cloud.base.notice.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.notice.entity.EmailErrorEntity;
import com.alinesno.cloud.base.notice.repository.EmailErrorRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
@NoRepositoryBean
public interface IEmailErrorService extends IBaseService<EmailErrorRepository, EmailErrorEntity, String> {

}
