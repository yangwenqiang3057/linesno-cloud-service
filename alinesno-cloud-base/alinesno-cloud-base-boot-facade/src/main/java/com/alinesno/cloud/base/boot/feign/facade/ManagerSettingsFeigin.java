package com.alinesno.cloud.base.boot.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;
import com.alinesno.cloud.base.boot.feign.dto.ManagerSettingsDto;

/**
 * <p> 参数配置表 请求客户端 </p>
 *
 * @author WeiXiaoJin
 * @since 2019-07-06 15:47:49
 */
@FeignClient(name="alinesno-cloud-base-boot" , path="managerSettings")
public interface ManagerSettingsFeigin extends IBaseFeign<ManagerSettingsDto> {


}
