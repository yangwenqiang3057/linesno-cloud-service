package com.alinesno.cloud.base.boot.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.alinesno.cloud.base.boot.feign.dto.ManagerCodeTypeDto;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2019-02-07 21:16:11
 */
@FeignClient(name="alinesno-cloud-base-boot" , path="managerCodeType")
public interface ManagerCodeTypeFeigin extends IBaseFeign<ManagerCodeTypeDto> {
	
	/**
	 * 通过代码类型查询代码
	 * @param asText
	 * @return
	 */
	@GetMapping("findByCodeTypeValue")
	ManagerCodeTypeDto findByCodeTypeValue(@RequestParam("codeTypeValue") String codeTypeValue);

//	public void deleteById(@RequestParam("id") String id);
}
