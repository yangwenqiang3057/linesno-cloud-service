package com.alinesno.cloud.base.workflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动入口
 * @EnableSwagger2 //开启swagger2 
 * @author LuoAnDong 
 * @since 2018-12-16 17:12:901
 */
@SpringBootApplication
public class AlinesnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlinesnoApplication.class, args);
	}
	
}
