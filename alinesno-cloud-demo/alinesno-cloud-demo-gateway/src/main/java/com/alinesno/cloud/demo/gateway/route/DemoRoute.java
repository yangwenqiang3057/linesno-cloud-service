package com.alinesno.cloud.demo.gateway.route;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
public class DemoRoute {

	@RequestMapping("/fallback")
	public Mono<String> fallback() {
		return Mono.just("fallback");
	}
	
	@Bean
	public RouteLocator myRoutes3(RouteLocatorBuilder builder) {
		return builder.routes().route(
					p -> p.path("/customer")
//					.uri("http://localhost:25008/")
					.uri("http://www.baidu.com/")
					)
				.build();
	}

	@Bean
	public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
	    return builder.routes()
	            .route("resource", r -> r.path("/resource")
	                    .uri("http://localhost:25008/"))
	            .build();
	}

//	@Bean
//	public RouteLocator myRoutes1(RouteLocatorBuilder builder) {
//		return builder.routes().route(
//				p -> p.path("/baidu").filters(f -> f.addRequestHeader("Hello", "World")).uri("http://www.baidu.com:80"))
//				.build();
//	}

	@Bean
	public RouteLocator myRoutes2(RouteLocatorBuilder builder) {
		return builder.routes().route(
				p -> p.path("/get").filters(f -> f.addRequestHeader("Hello", "World")).uri("http://httpbin.org:80"))
				.route(p -> p.host("*.hystrix.com").filters(f -> f.hystrix(config -> config.setName("mycmd")))
						.uri("http://httpbin.org:80"))
				.build();
	}
}
