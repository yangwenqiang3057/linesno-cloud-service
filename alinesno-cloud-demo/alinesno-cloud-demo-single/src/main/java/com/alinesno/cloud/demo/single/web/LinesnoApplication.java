package com.alinesno.cloud.demo.single.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 启动入口
 * @author LuoAnDong
 * @since 2018年8月7日 上午8:45:02
 */
// @SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@SpringBootApplication
@EnableAsync        //开启异步任务
public class LinesnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(LinesnoApplication.class, args);
	}
	
}
