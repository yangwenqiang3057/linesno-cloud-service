package com.alinesno.cloud.demo.base.rest.condiction;

import java.util.Collection;
import java.util.Map;

import javax.persistence.criteria.Predicate ;

import org.springframework.data.jpa.domain.Specification; 

/**
 * 公共的条件封装对象，使用页面使用
 * 
 * @author LuoAnDong
 * @since 2018年12月14日 下午12:03:13
 * @param <T>
 */
public interface Wrapper<T> extends Specification<T> {

	/**
	 * <p>
	 * SQL中WHERE关键字跟的条件语句
	 * </p>
	 * <p>
	 * eg: ew.where("name='zhangsan'").where(id!=null, "id={0}", id);
	 * <p>
	 * 输出:<br>
	 * 如果id=123: WHERE (NAME='zhangsan' AND id=123)<br>
	 * 如果id=null: WHERE (NAME='zhangsan')
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param sqlWhere
	 *            where语句
	 * @param params
	 *            参数集
	 * @return this
	 */
	Predicate where(boolean condition, String sqlWhere, Object... params);

	/**
	 * <p>
	 * SQL中WHERE关键字跟的条件语句
	 * </p>
	 * <p>
	 * eg: ew.where("name='zhangsan'").where("id={0}","123");
	 * <p>
	 * 输出: WHERE (NAME='zhangsan' AND id=123)
	 * </p>
	 *
	 * @param sqlWhere
	 *            where语句
	 * @param params
	 *            参数集
	 * @return this
	 */
	Predicate where(String sqlWhere, Object... params);

	/**
	 * <p>
	 * 等同于SQL的"field=value"表达式
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param column
	 * @param params
	 * @return
	 */
	Predicate eq(boolean condition, String column, Object params);

	/**
	 * <p>
	 * 等同于SQL的"field=value"表达式
	 * </p>
	 *
	 * @param column
	 * @param params
	 * @return
	 */
	Predicate eq(String column, Object params);

	/**
	 * <p>
	 * 等同于SQL的"field <> value"表达式
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param column
	 * @param params
	 * @return
	 */
	Predicate ne(boolean condition, String column, Object params);

	/**
	 * <p>
	 * 等同于SQL的"field <> value"表达式
	 * </p>
	 *
	 * @param column
	 * @param params
	 * @return
	 */
	Predicate ne(String column, Object params);

	/**
	 * <p>
	 * 等同于SQL的"field=value"表达式
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param params
	 * @return
	 */
	Predicate allEq(boolean condition, Map<String, Object> params);

	/**
	 * <p>
	 * 等同于SQL的"field=value"表达式
	 * </p>
	 *
	 * @param params
	 * @return
	 */
	Predicate allEq(Map<String, Object> params);

	/**
	 * <p>
	 * 等同于SQL的"field>value"表达式
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param column
	 * @param params
	 * @return
	 */
	Predicate gt(boolean condition, String column, Object params);

	/**
	 * <p>
	 * 等同于SQL的"field>value"表达式
	 * </p>
	 *
	 * @param column
	 * @param params
	 * @return
	 */
	Predicate gt(String column, Object params);

	/**
	 * <p>
	 * 等同于SQL的"field>=value"表达式
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param column
	 * @param params
	 * @return
	 */
	Predicate ge(boolean condition, String column, Object params);

	/**
	 * <p>
	 * 等同于SQL的"field>=value"表达式
	 * </p>
	 *
	 * @param column
	 * @param params
	 * @return
	 */
	Predicate ge(String column, Object params);

	/**
	 * <p>
	 * 等同于SQL的"field<value"表达式
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param column
	 * @param params
	 * @return
	 */
	Predicate lt(boolean condition, String column, Object params);

	/**
	 * <p>
	 * 等同于SQL的"field<value"表达式
	 * </p>
	 *
	 * @param column
	 * @param params
	 * @return
	 */
	Predicate lt(String column, Object params);

	/**
	 * <p>
	 * 等同于SQL的"field<=value"表达式
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param column
	 * @param params
	 * @return
	 */
	Predicate le(boolean condition, String column, Object params);

	/**
	 * <p>
	 * 等同于SQL的"field<=value"表达式
	 * </p>
	 *
	 * @param column
	 * @param params
	 * @return
	 */
	Predicate le(String column, Object params);

	/**
	 * <p>
	 * AND 连接后续条件
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param sqlAnd
	 *            and条件语句
	 * @param params
	 *            参数集
	 * @return this
	 */
	Predicate and(boolean condition, String sqlAnd, Object... params);

	/**
	 * <p>
	 * AND 连接后续条件
	 * </p>
	 *
	 * @param sqlAnd
	 *            and条件语句
	 * @param params
	 *            参数集
	 * @return this
	 */
	Predicate and(String sqlAnd, Object... params);

	/**
	 * <p>
	 * 使用AND连接并换行
	 * </p>
	 * <p>
	 * eg: ew.where("name='zhangsan'").and("id=11").andNew("statu=1"); 输出： WHERE
	 * (name='zhangsan' AND id=11) AND (statu=1)
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param sqlAnd
	 *            AND 条件语句
	 * @param params
	 *            参数值
	 * @return this
	 */
	Predicate andNew(boolean condition, String sqlAnd, Object... params);

	/**
	 * <p>
	 * 使用AND连接并换行
	 * </p>
	 * <p>
	 * eg: ew.where("name='zhangsan'").and("id=11").andNew("statu=1"); 输出： WHERE
	 * (name='zhangsan' AND id=11) AND (statu=1)
	 * </p>
	 *
	 * @return this
	 */
	Predicate andNew();

	/**
	 * <p>
	 * 使用AND连接并换行
	 * </p>
	 * <p>
	 * eg: ew.where("name='zhangsan'").and("id=11").andNew("statu=1"); 输出： WHERE
	 * (name='zhangsan' AND id=11) AND (statu=1)
	 * </p>
	 *
	 * @param sqlAnd
	 *            AND 条件语句
	 * @param params
	 *            参数值
	 * @return this
	 */
	Predicate andNew(String sqlAnd, Object... params);

	/**
	 * <p>
	 * 使用AND连接并换行
	 * </p>
	 * <p>
	 *
	 * @return this
	 */
	Predicate and();

	/**
	 * <p>
	 * 使用OR连接并换行
	 * </p>
	 *
	 * @return this
	 */
	Predicate or();

	/**
	 * <p>
	 * 添加OR条件
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param sqlOr
	 *            or 条件语句
	 * @param params
	 *            参数集
	 * @return this
	 */
	Predicate or(boolean condition, String sqlOr, Object... params);

	/**
	 * <p>
	 * 添加OR条件
	 * </p>
	 *
	 * @param sqlOr
	 *            or 条件语句
	 * @param params
	 *            参数集
	 * @return this
	 */
	Predicate or(String sqlOr, Object... params);

	/**
	 * <p>
	 * 使用OR换行，并添加一个带()的新的条件
	 * </p>
	 * <p>
	 * eg: ew.where("name='zhangsan'").and("id=11").orNew("statu=1"); 输出： WHERE
	 * (name='zhangsan' AND id=11) OR (statu=1)
	 * </p>
	 *
	 * @return this
	 */
	Predicate orNew();

	/**
	 * <p>
	 * 使用OR换行，并添加一个带()的新的条件
	 * </p>
	 * <p>
	 * eg: ew.where("name='zhangsan'").and("id=11").orNew("statu=1"); 输出： WHERE
	 * (name='zhangsan' AND id=11) OR (statu=1)
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param sqlOr
	 *            AND 条件语句
	 * @param params
	 *            参数值
	 * @return this
	 */
	Predicate orNew(boolean condition, String sqlOr, Object... params);

	/**
	 * <p>
	 * 使用OR换行，并添加一个带()的新的条件
	 * </p>
	 * <p>
	 * eg: ew.where("name='zhangsan'").and("id=11").orNew("statu=1"); 输出： WHERE
	 * (name='zhangsan' AND id=11) OR (statu=1)
	 * </p>
	 *
	 * @param sqlOr
	 *            AND 条件语句
	 * @param params
	 *            参数值
	 * @return this
	 */
	Predicate orNew(String sqlOr, Object... params);

	/**
	 * <p>
	 * SQL中groupBy关键字跟的条件语句
	 * </p>
	 * <p>
	 * eg: ew.where("name='zhangsan'").groupBy("id,name")
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param columns
	 *            SQL 中的 Group by 语句，无需输入 Group By 关键字
	 * @return this
	 */
	Predicate groupBy(boolean condition, String columns);

	/**
	 * <p>
	 * SQL中groupBy关键字跟的条件语句
	 * </p>
	 * <p>
	 * eg: ew.where("name='zhangsan'").groupBy("id,name")
	 * </p>
	 *
	 * @param columns
	 *            SQL 中的 Group by 语句，无需输入 Group By 关键字
	 * @return this
	 */
	Predicate groupBy(String columns);

	/**
	 * <p>
	 * SQL中having关键字跟的条件语句
	 * </p>
	 * <p>
	 * eg: ew.groupBy("id,name").having("id={0}",22).and("password is not null")
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param sqlHaving
	 *            having关键字后面跟随的语句
	 * @param params
	 *            参数集
	 * @return EntityWrapper<T>
	 */
	Predicate having(boolean condition, String sqlHaving, Object... params);

	/**
	 * <p>
	 * SQL中having关键字跟的条件语句
	 * </p>
	 * <p>
	 * eg: ew.groupBy("id,name").having("id={0}",22).and("password is not null")
	 * </p>
	 *
	 * @param sqlHaving
	 *            having关键字后面跟随的语句
	 * @param params
	 *            参数集
	 * @return EntityWrapper<T>
	 */
	Predicate having(String sqlHaving, Object... params);

	/**
	 * <p>
	 * SQL中orderby关键字跟的条件语句
	 * </p>
	 * <p>
	 * eg: ew.groupBy("id,name").having("id={0}",22).and("password is not null"
	 * ).orderBy("id,name")
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param columns
	 *            SQL 中的 order by 语句，无需输入 Order By 关键字
	 * @return this
	 */
	Predicate orderBy(boolean condition, String columns);

	/**
	 * <p>
	 * SQL中orderby关键字跟的条件语句
	 * </p>
	 * <p>
	 * eg: ew.groupBy("id,name").having("id={0}",22).and("password is not null"
	 * ).orderBy("id,name")
	 * </p>
	 *
	 * @param columns
	 *            SQL 中的 order by 语句，无需输入 Order By 关键字
	 * @return this
	 */
	Predicate orderBy(String columns);

	/**
	 * <p>
	 * SQL中orderby关键字跟的条件语句，可根据变更动态排序
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param columns
	 *            SQL 中的 order by 语句，无需输入 Order By 关键字
	 * @param isAsc
	 *            是否为升序
	 * @return this
	 */
	Predicate orderBy(boolean condition, String columns, boolean isAsc);

	/**
	 * <p>
	 * SQL中orderby关键字跟的条件语句，可根据变更动态排序
	 * </p>
	 *
	 * @param columns
	 *            SQL 中的 order by 语句，无需输入 Order By 关键字
	 * @param isAsc
	 *            是否为升序
	 * @return this
	 */
	Predicate orderBy(String columns, boolean isAsc);

	/**
	 * <p>
	 * LIKE条件语句，value中无需前后%
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param column
	 *            字段名称
	 * @param value
	 *            匹配值
	 * @return this
	 */
	Predicate like(boolean condition, String column, String value);

	/**
	 * <p>
	 * LIKE条件语句，value中无需前后%
	 * </p>
	 *
	 * @param column
	 *            字段名称
	 * @param value
	 *            匹配值
	 * @return this
	 */
	Predicate like(String column, String value);

	/**
	 * <p>
	 * NOT LIKE条件语句，value中无需前后%
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param column
	 *            字段名称
	 * @param value
	 *            匹配值
	 * @return this
	 */
	Predicate notLike(boolean condition, String column, String value);

	/**
	 * <p>
	 * NOT LIKE条件语句，value中无需前后%
	 * </p>
	 *
	 * @param column
	 *            字段名称
	 * @param value
	 *            匹配值
	 * @return this
	 */
	Predicate notLike(String column, String value);

	/**
	 * <p>
	 * LIKE条件语句，value中无需前后%
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param column
	 *            字段名称
	 * @param value
	 *            匹配值
	 * @param type
	 * @return this
	 */
	Predicate like(boolean condition, String column, String value, CondictionLike type);

	/**
	 * <p>
	 * LIKE条件语句，value中无需前后%
	 * </p>
	 *
	 * @param column
	 *            字段名称
	 * @param value
	 *            匹配值
	 * @param type
	 * @return this
	 */
	Predicate like(String column, String value, CondictionLike type);

	/**
	 * <p>
	 * NOT LIKE条件语句，value中无需前后%
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param column
	 *            字段名称
	 * @param value
	 *            匹配值
	 * @param type
	 * @return this
	 */
	Predicate notLike(boolean condition, String column, String value, CondictionLike type);

	/**
	 * <p>
	 * NOT LIKE条件语句，value中无需前后%
	 * </p>
	 *
	 * @param column
	 *            字段名称
	 * @param value
	 *            匹配值
	 * @param type
	 * @return this
	 */
	Predicate notLike(String column, String value, CondictionLike type);

	/**
	 * <p>
	 * is not null 条件
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param columns
	 *            字段名称。多个字段以逗号分隔。
	 * @return this
	 */
	Predicate isNotNull(boolean condition, String columns);

	/**
	 * <p>
	 * is not null 条件
	 * </p>
	 *
	 * @param columns
	 *            字段名称。多个字段以逗号分隔。
	 * @return this
	 */
	Predicate isNotNull(String columns);

	/**
	 * <p>
	 * is not null 条件
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param columns
	 *            字段名称。多个字段以逗号分隔。
	 * @return this
	 */
	Predicate isNull(boolean condition, String columns);

	/**
	 * <p>
	 * is not null 条件
	 * </p>
	 *
	 * @param columns
	 *            字段名称。多个字段以逗号分隔。
	 * @return this
	 */
	Predicate isNull(String columns);

	/**
	 * <p>
	 * EXISTS 条件语句，目前适配mysql及oracle
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param value
	 *            匹配值
	 * @return this
	 */
	Predicate exists(boolean condition, String value);

	/**
	 * <p>
	 * EXISTS 条件语句，目前适配mysql及oracle
	 * </p>
	 *
	 * @param value
	 *            匹配值
	 * @return this
	 */
	Predicate exists(String value);

	/**
	 * <p>
	 * NOT EXISTS条件语句
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param value
	 *            匹配值
	 * @return this
	 */
	Predicate notExists(boolean condition, String value);

	/**
	 * <p>
	 * NOT EXISTS条件语句
	 * </p>
	 *
	 * @param value
	 *            匹配值
	 * @return this
	 */
	Predicate notExists(String value);

	/**
	 * <p>
	 * IN 条件语句，目前适配mysql及oracle
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param column
	 *            字段名称
	 * @param value
	 *            逗号拼接的字符串
	 * @return this
	 */
	Predicate in(boolean condition, String column, String value);

	/**
	 * <p>
	 * IN 条件语句，目前适配mysql及oracle
	 * </p>
	 *
	 * @param column
	 *            字段名称
	 * @param value
	 *            逗号拼接的字符串
	 * @return this
	 */
	Predicate in(String column, String value);

	/**
	 * <p>
	 * NOT IN条件语句
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param column
	 *            字段名称
	 * @param value
	 *            逗号拼接的字符串
	 * @return this
	 */
	Predicate notIn(boolean condition, String column, String value);

	/**
	 * <p>
	 * NOT IN条件语句
	 * </p>
	 *
	 * @param column
	 *            字段名称
	 * @param value
	 *            逗号拼接的字符串
	 * @return this
	 */
	Predicate notIn(String column, String value);

	/**
	 * <p>
	 * IN 条件语句，目前适配mysql及oracle
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param column
	 *            字段名称
	 * @param value
	 *            匹配值 List集合
	 * @return this
	 */
	Predicate in(boolean condition, String column, Collection<?> value);

	/**
	 * <p>
	 * IN 条件语句，目前适配mysql及oracle
	 * </p>
	 *
	 * @param column
	 *            字段名称
	 * @param value
	 *            匹配值 List集合
	 * @return this
	 */
	Predicate in(String column, Collection<?> value);

	/**
	 * <p>
	 * NOT IN 条件语句，目前适配mysql及oracle
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param column
	 *            字段名称
	 * @param value
	 *            匹配值 List集合
	 * @return this
	 */
	Predicate notIn(boolean condition, String column, Collection<?> value);

	/**
	 * <p>
	 * NOT IN 条件语句，目前适配mysql及oracle
	 * </p>
	 *
	 * @param column
	 *            字段名称
	 * @param value
	 *            匹配值 List集合
	 * @return this
	 */
	Predicate notIn(String column, Collection<?> value);

	/**
	 * <p>
	 * IN 条件语句，目前适配mysql及oracle
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param column
	 *            字段名称
	 * @param value
	 *            匹配值 object数组
	 * @return this
	 */
	Predicate in(boolean condition, String column, Object[] value);

	/**
	 * <p>
	 * IN 条件语句，目前适配mysql及oracle
	 * </p>
	 *
	 * @param column
	 *            字段名称
	 * @param value
	 *            匹配值 object数组
	 * @return this
	 */
	Predicate in(String column, Object[] value);

	/**
	 * <p>
	 * NOT IN 条件语句，目前适配mysql及oracle
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param column
	 *            字段名称
	 * @param value
	 *            匹配值 object数组
	 * @return this
	 */
	Predicate notIn(boolean condition, String column, Object... value);

	/**
	 * <p>
	 * NOT IN 条件语句，目前适配mysql及oracle
	 * </p>
	 *
	 * @param column
	 *            字段名称
	 * @param value
	 *            匹配值 object数组
	 * @return this
	 */
	Predicate notIn(String column, Object... value);

	/**
	 * <p>
	 * betwwee 条件语句
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param column
	 *            字段名称
	 * @param val1
	 * @param val2
	 * @return this
	 */
	Predicate between(boolean condition, String column, Object val1, Object val2);

	/**
	 * <p>
	 * betwwee 条件语句
	 * </p>
	 *
	 * @param column
	 *            字段名称
	 * @param val1
	 * @param val2
	 * @return this
	 */
	Predicate between(String column, Object val1, Object val2);

	/**
	 * <p>
	 * NOT betwwee 条件语句
	 * </p>
	 *
	 * @param condition
	 *            拼接的前置条件
	 * @param column
	 *            字段名称
	 * @param val1
	 * @param val2
	 * @return this
	 */
	Predicate notBetween(boolean condition, String column, Object val1, Object val2);

	/**
	 * <p>
	 * NOT betwwee 条件语句
	 * </p>
	 *
	 * @param column
	 *            字段名称
	 * @param val1
	 * @param val2
	 * @return this
	 */
	Predicate notBetween(String column, Object val1, Object val2);

}