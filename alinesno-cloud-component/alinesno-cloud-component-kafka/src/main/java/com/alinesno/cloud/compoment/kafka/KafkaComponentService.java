package com.alinesno.cloud.compoment.kafka;

/**
 * kafka组件服务  
 * @author LuoAnDong
 * @since 2019年4月9日 上午11:01:54
 */
public interface KafkaComponentService {

	/**
	 * 异步发送消息
	 * @param topic
	 * @param data 
	 */
	public void asyncSendMessage(String topic , String data) ; 

	/**
	 * 异步发送消息
	 * @param topic
	 * @param key 可理解成主键
	 * @param data
	 */
	public void asyncSendMessage(String topic , String key , String data) ; 

	/**
	 * 同步发送消息
	 * @param topic
	 * @param data 
	 * @return 成功 true | false 失败
	 */
	public boolean blockSendMessage(String topic , String data) ; 
	
	/**
	 * 同步发送消息
	 * @param topic
	 * @param key 可理解成主键
	 * @param data
	 * @return
	 */
	public boolean blockSendMessage(String topic , String key , String data) ; 
	
}
